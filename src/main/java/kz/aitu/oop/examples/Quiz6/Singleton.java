package kz.aitu.oop.examples.Quiz6;

public class Singleton {
    private static Singleton single_instance;
    public String str;
    private Singleton() {}
    public static synchronized Singleton getSingleInstance()
    {
        if (single_instance == null)
            single_instance = new Singleton();

        return single_instance;
    }
}
