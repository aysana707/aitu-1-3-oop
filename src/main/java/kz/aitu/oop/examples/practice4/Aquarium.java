package kz.aitu.oop.examples.practice4;

public class Aquarium {
    private String name;
    public Aquarium(String name){
        this.name=name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    @Override
    public String toString() {
        return "Aquarium{" +
                "name='" + name + '\'' +
                '}';
    }

}
