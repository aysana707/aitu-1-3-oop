package kz.aitu.oop.examples.practice4;

public class Construction extends Aquarium{
    private String denomination;
    public Construction(String denomination) {
        super(denomination);
        this.denomination=denomination;
    }

    public void setDenomination(String denomination) {
        this.denomination = denomination;
    }

    public String getDenomination() {
        return denomination;
    }

    @Override
    public String toString() {
        return "Construction{" +
                "denomination='" + denomination + '\'' +
                '}';
    }

}
