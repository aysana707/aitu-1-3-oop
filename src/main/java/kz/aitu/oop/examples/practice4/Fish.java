package kz.aitu.oop.examples.practice4;

public class Fish extends Aquarium {
    private int age;
    private String gender;
    public Fish(String title, int age, String gender) {
        super(title);
        this.age=age;
        this.gender=gender;
    }

    public int getAge() {
        return age;
    }

    public String getGender() {
        return gender;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    @Override
    public String toString() {
        return "FIsh{" +
                "age=" + age +
                ", gender='" + gender + '\'' +
                '}';
    }

}
