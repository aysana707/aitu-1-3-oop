package kz.aitu.oop.examples.practice5;

public class Cost extends Stone {
    private int cost;
    public Cost(String name, int weight, int cost) {
        super(name, weight);
        this.cost=cost;
    }

    public void setCost(int cost) {
        this.cost = cost;
    }

    public int getCost() {
        return cost;
    }

    @Override
    public String toString() {
        return "Value{" +
                "cost=" + cost +
                '}';
    }

}
