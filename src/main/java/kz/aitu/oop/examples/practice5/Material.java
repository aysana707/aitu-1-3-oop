package kz.aitu.oop.examples.practice5;

public class Material extends Necklace{
    private String title;

    public Material(String name, int carat, String Title) {
        super(name, carat);
        this.title=title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getTitle() {
        return title;
    }

    @Override
    public String toString() {
        return "Material{" +
                "title='" + title + '\'' +
                '}';
    }

}
