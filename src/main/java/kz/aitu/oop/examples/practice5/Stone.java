package kz.aitu.oop.examples.practice5;

public class Stone {
    private String name;
    private int carats;
    public Stone(String name, int carats){
        this.name=name;
        this.carats=carats;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setCarats(int carats) {
        this.carats = carats;
    }

    public String getName() {
        return name;
    }

    public int getCarats() {
        return carats;
    }

    @Override
    public String toString() {
        return "Gemstone{" +
                "name='" + name + '\'' +
                ", carats=" + carats +
                '}';
    }

}
