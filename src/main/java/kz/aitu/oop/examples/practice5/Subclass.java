package kz.aitu.oop.examples.practice5;

public class Subclass extends Stone {
    private String subclass;

    public Subclass(String name, int weight, String subclass) {
        super(name, weight);
        this.subclass=subclass;
    }

    public void setSubclass(String subclass) {
        this.subclass = subclass;
    }

    public String getSubclass() {
        return subclass;
    }

}
