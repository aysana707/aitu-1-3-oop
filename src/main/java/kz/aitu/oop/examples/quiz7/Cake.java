package kz.aitu.oop.examples.quiz7;

public class Cake implements Food {
    @Override
    public void getType() {
        System.out.println("The factory returned class Cake\nSomeone ordered a Dessert!");
    }
}
