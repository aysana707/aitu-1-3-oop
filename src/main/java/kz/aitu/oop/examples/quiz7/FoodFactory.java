package kz.aitu.oop.examples.quiz7;

public class FoodFactory {
    public Object getFood(String s){
        if (s.equals("cake")){
            Cake c = new Cake();
            c.getType();
            return c;
        }
        if (s.equals("pizza")){
            Pizza p = new Pizza();
            p.getType();
            return p;
        }
        return null;
    }
}
