package kz.aitu.oop.examples.quiz7;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        FoodFactory foodFactory = new FoodFactory();

        Scanner sc = new Scanner(System.in);
        String s = sc.nextLine();
        foodFactory.getFood(s);
    }
}
