package kz.aitu.oop.week3;

public class Circle extends Shape {
    private Double radius;

    public Circle() {
        radius = 1.0;
    }

    public Circle(Double r) {
        radius = r;
    }

    public Circle(Double r, String color, Boolean filled) {
        super(color, filled);
        radius = r;

    }

    public Double getRadius() {
        return radius;
    }

    public void setRadius(Double radius) {
        this.radius = radius;
    }

    public Double getArea() {
        Double r;
        r = getRadius();
        Double a;
        a = Math.PI * r * r;
        return a;
    }

    public Double getPerimeter() {
        Double r;
        r = getRadius();
        Double a;
        a = Math.PI * 2 * r;
        return a;
    }

    @Override
    public String toString() {
        return "Circle[" + super.toString() + " , radius =" + radius + "]";
    }

    public static void main(String[] args) {
        Circle a= new Circle(2.0);
        System.out.println(a.toString());
    }
}
