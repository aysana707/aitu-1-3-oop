package kz.aitu.oop.week3;

public class Rectangle extends Shape {
    private Double width;
    private Double length;
    public Rectangle(){
        width=1.0;
        length=1.0;
    }
    public Rectangle(Double w, Double l){
        width=w;
        length=l;
    }
    public Rectangle(Double w,Double l, String color, Boolean filled ){
        super(color,filled);
        width=w;
        length=l;

    }

    public Double getWidth() {
        return width;
    }

    public void setWidth(Double width) {
        this.width = width;
    }

    public Double getLength() {
        return length;
    }

    public void setLength(Double length) {
        this.length = length;
    }
    public Double getArea(){
        Double  w,l;
        l=getLength();
        w=getWidth();
        Double a;
        a= l*w;
        return a;
    }

    public Double getPerimeter(){
        Double  w,l;
        l=getLength();
        w=getWidth();
        Double a;
        a= (l+w)*2;
        return a;
    }

    @Override
    public String toString() {
        return "Rectangle["+super.toString()+ ",width="+ width + ",length="+length + "]";
    }
    public static void main(String[] args) {
        Rectangle b = new Rectangle();
        System.out.println(b.toString());
    }
}
