package kz.aitu.oop.week3;

public class Shape {
    private String color;
    private Boolean filled;

    public Shape(String c, Boolean f) {
        color = c;
        filled = f;

    }

    public Shape() {
        color = "green";
        filled = true;

    }

    public Boolean isFilled() {
        return filled;
    }

    public void setFilled(Boolean filled) {
        this.filled = filled;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }


    public String toString() {
        return "Shape[" + "color= " + color + ",filled=" + filled + "]";
    }

    public static void main(String[] args) {
        Shape b = new Shape();
        b.setColor("blue");
        System.out.println(b.getColor());
        b.setFilled(true);
        System.out.println(b.isFilled());
        System.out.println(b.toString());
    }
}
