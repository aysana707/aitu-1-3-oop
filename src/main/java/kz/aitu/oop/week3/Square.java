package kz.aitu.oop.week3;

public class Square extends Rectangle {
    public Square(){ super();}
    public Square(Double s){
        super(s,s);
    }
    public Square(Double s, String color, Boolean filled ){
        super(s,s,color,filled);
    }
    @Override
    public String toString() {
        return "Square[" + super.toString()+ "]";
    }

    @Override
    public Double getArea() {
        return super.getArea();
    }

    @Override
    public Double getPerimeter() {
        return super.getPerimeter();
    }

    @Override
    public void setWidth(Double width) {
        super.setWidth(width);
    }

    @Override
    public void setLength(Double length) {
        super.setLength(length);
    }

    public static void main(String[] args) {
        Square c=new Square(1.0,"black", true);
        c.setLength(3.0);
        c.setWidth(3.0);
        System.out.println(c.toString());
        System.out.println(c.getArea());
        System.out.println(c.getPerimeter());
    }
}
